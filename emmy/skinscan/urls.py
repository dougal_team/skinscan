from django.urls import path

from skinscan.logic import ModelManager
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('feedback', views.feedback, name='feedback'),
    path('upload', views.upload_photo, name='upload')
]

# loading model on init
#ModelManager.load_model()
