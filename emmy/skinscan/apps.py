from django.apps import AppConfig

from skinscan.logic import ModelManager


class SkinscanConfig(AppConfig):
    name = 'skinscan'
