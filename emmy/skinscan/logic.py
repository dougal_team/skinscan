import json
import logging
import os
import threading
from io import BytesIO

import numpy as np
from PIL import Image
from django.core.files.storage import FileSystemStorage
from keras import optimizers

from keras.preprocessing import image
from keras.applications.inception_v3 import preprocess_input
from keras.models import model_from_json

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

logger = logging.getLogger('django')


def handle_uploaded_file(f):
    """
    Saving file to file system and returns a name
    :param f:
    :return:
    """
    fss = FileSystemStorage(location='./skinscan/photos/')
    filepath = fss.get_available_name('photo.jpeg')

    logger.debug('handle_uploaded_file() - saving file {}'.format(filepath))

    full_path = "./skinscan/photos/{}".format(filepath)
    with open(full_path, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

    return filepath


def get_response_json(prediction):
    """
    Returns json response according to prediction
    :param prediction:
    :return:
    """
    mel_prob = prediction[0]
    nev_prob = prediction[1]
    unk_prob = prediction[2]

    description = None
    predicted_name = None

    if nev_prob >= 0.5:
        if mel_prob < 0.5 and unk_prob < 0.5:
            predicted_name = "Simple Nevus"
            description = "Risk of cancer is very low, looks like regular nevus"
        elif nev_prob > mel_prob and nev_prob > unk_prob:
            predicted_name = "Simple Nevus"
            description = "Looks like nevus, but risk is still high"
        elif mel_prob >= nev_prob:
            predicted_name = "Melanoma"
            description = "Looks like risk of melanoma is very high"
        else:
            predicted_name = "Unknown"
            description = "Looks like it's not simple nevus, better consult with doctor"

    else:
        if mel_prob >= 0.5 and unk_prob < 0.5:
            predicted_name = "Melanoma"
            description = "Risk of melanoma is high, please consult with doctor"
        elif mel_prob > unk_prob:
            predicted_name = "Melanoma"
            description = "Risk of cancer is high, please consult with doctor"
        else:
            predicted_name = "Unknown"
            description = "Risk of cancer is high, please consult with doctor"

    response_data = {'mel_prob': str(round(mel_prob*100,2)),
                     'nev_prob': str(round(nev_prob*100, 2)),
                     'unk_prob': str(round(unk_prob*100, 2)),
                     'description': description,
                     'predicted_name': predicted_name }

    return json.dumps(response_data)



class ModelManager:
    """
    ModelManager - container for keras models
    """

    _model = None
    _model_lock = threading.Lock()

    @classmethod
    def load_model(cls):
        """
        Loading model from local file storage
        :return: True for success, False on error
        """
        try:
            logger.debug('load_model() - starting')
            cls._model_lock.acquire()
            if cls._model is None:
                model_json = open(os.path.join(PROJECT_ROOT, 'pred_models/model.json'))

                loaded_model_json = model_json.read()
                model_json.close()
                cls._model = model_from_json(loaded_model_json)

                cls._model.load_weights(os.path.join(PROJECT_ROOT, "pred_models/weights-skinscan-36-0.82.hdf5"))

                # evaluate loaded model on test data
                adam_op = optimizers.Adam(lr=0.0001)
                cls._model.compile(loss='categorical_crossentropy', optimizer=adam_op, metrics=['acc'])

            logger.info('load_model() - model is loadded')
            cls._model_lock.release()

            return True

        except Exception as exc:
            logger.error('load_model() - error: ', exc)
            return False

    @classmethod
    def predict(cls, image_base):
        """
        Making prediction for photo and returns probability
        for melanoma, nevus, unknown
        :return: list with probs, example: [0.54, 0.78, 0.6], None for error
        """
        try:
            if cls._model is None:
                if cls.load_model() is None:
                    return False

            #img_file = BytesIO(image_base)
            image_pil = Image.open(image_base)
            logger.debug('predict() - loading image')
            #image_pil = image.load_img('./skinscan/photos/{}'.format(image_base), target_size=(155, 155))
            hw_tuple = (155, 155)
            if image_pil.size != hw_tuple:
                image_pil = image_pil.resize(hw_tuple)

            x = image.img_to_array(image_pil)
            x = np.expand_dims(x, axis=0)
            x = preprocess_input(x)

            preds = cls._model.predict(x)[0]
            logger.info('predict() - prediction is finished')

            return preds

        except Exception as exc:
            logger.error('predict() - error:{}'.format(exc))
            return None
