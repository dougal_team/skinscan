from django.db import models
from django.utils.timezone import now

# Create your models here.
class Feedback(models.Model):
    """
    Model for users feedback on first page
    """
    feedback_text = models.CharField("Feedback text", max_length=1000)
    pub_date = models.DateTimeField("Date Created", default=now)
    remote_addr = models.CharField(max_length=20)

    def __str__(self):
        return self.feedback_text[:50]
