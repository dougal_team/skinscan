# Generated by Django 2.0.6 on 2018-10-20 12:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('skinscan', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feedback',
            name='pub_date',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
