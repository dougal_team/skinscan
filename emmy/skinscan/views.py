from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from skinscan.logic import ModelManager

from skinscan.forms import FileUploadForm

from skinscan.logic import handle_uploaded_file

from skinscan.logic import get_response_json
from .models import Feedback
import logging

logger = logging.getLogger('django')

@require_http_methods(['GET'])
def index(request):
    """
    Main page controller
    :param request:
    :return: index make markup
    """
    template = loader.get_template('skinscan/index.html')
    context = {}
    logger.info("index() - view called")

    return HttpResponse(template.render(context, request))


@require_http_methods(['POST'])
def feedback(request):
    """
    Saving feedback text in database
    :param request:
    :return:
    """
    try:
        text = request.POST.get('feedbackText')
        new_feedback = Feedback(feedback_text=text,
                                remote_addr=request.META.get('REMOTE_ADDR'))
        logger.info("feedback() - saving feedback from {}".format(text))

        new_feedback.save()
    except Exception as exc:
        print('Error: ', exc)
        logger.error("feedback() - error occurred: {}".format(exc))
        return HttpResponse(status=500)

    return HttpResponse(status=200)


@csrf_exempt
@require_http_methods(['POST'])
def upload_photo(request):
    """
    Example for view for handler
    :param request:
    :return:
    """
    logger.info('upload_photo() - request received')
    photo_file = request.FILES.get('file')

    if photo_file is None:
        logger.warning('upload_photo() - ho photo loaded')
        return HttpResponse(status=400)

    #filepath = handle_uploaded_file(request.FILES['file'])
    result = ModelManager.predict(photo_file.file)
    logger.info('upload_photo() - prediction result {}'.format(result))

    if result is None:
        return HttpResponse(500)
    else:
        res_json = get_response_json(result)
        return HttpResponse(res_json, content_type='application/json', status=200)

    # photo_data = photo_file.read()

    # result = ModelManager.predict(photo_data)
    # if result is None:
    #    return HttpResponse(status=500)

    # response_data = {'melanoma': result[0], 'nevus': result[1], 'other': result[2]}
    # return HttpResponse(json.dumps(response_data), content_type='application/json', status=200)
