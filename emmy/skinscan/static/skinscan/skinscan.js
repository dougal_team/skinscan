// Hiding result by default

// Function for loading feedback with AJAX on server
$(function(){
    $('#btnSaveFeedback').click(function(event) {

        event.preventDefault();

        var feedbackText = $("#tbFeedback").val();
        $.ajax({
            type: 'POST',
            data: {'feedbackText':feedbackText,
                   'csrfmiddlewaretoken': getCookie('csrftoken')
                   },

            success: function()
            {
                $( "#pnlFeedbackResult" ).append('<div class="alert alert-success" role="alert">Thanks for your feedback!</div>');
                $("#tbFeedback").val("");
            },
            error: function() {
                $( "#pnlFeedbackResult" )
                .append('<div class="alert alert-danger" role="alert">For some reason we can not save your feedback, please send email!</div>');
            },
            url: './feedback',
            cache: false
        });
        return false;
    });
}

);

$(function(){
    // handling changed file
    $("#inpPhotoFile").change(function (){
           window.location.hash = "#upload";
    });
});


/// Function for loading photo on server
$(function(){
    $('#btnUpload').click(function(event) {

        event.preventDefault();

        var fileInput = document.getElementById('inpPhotoFile');
        var file = fileInput.files[0];

        if(file == null)
        {
            alert('Please select a photo first!');
            return false;
        }

        var formData = new FormData();
        formData.append('file', file);

        $.ajax({
            type: 'POST',
            enctype: 'multipart/form-data',
            contentType: false,
            processData: false,
            method: 'POST',
            type: 'POST', // For jQuery < 1.9
            cache: false,
            data: formData,
            //{'photoFiles': data,
            //       'csrfmiddlewaretoken': getCookie('csrftoken')
            //       },

            success: function(data)
            {
                $('#divResults').empty();
                $('<strong class="d-inline-block mb-2 text-primary" id="lblResultPred">'
                + data.predicted_name +'</strong>').appendTo('#divResults');

                $('<p class="card-text mb-auto">Melanoma risk:' + data.mel_prob +'%<br/>Simple nevus: '
                + data.nev_prob + ' %<br/>Unknown nevus:' + data.unk_prob +' %<br/><small>'
                + data.description + '</small></p>').appendTo('#divResults');

                $('#result').show();

                var preview = document.querySelector('#imgPreview');
                var file    = document.querySelector('input[type=file]').files[0];
                var reader  = new FileReader();

                  reader.onloadend = function () {
                    preview.src = reader.result;
                  }

                  if (file) {
                    reader.readAsDataURL(file);
                  } else {
                    preview.src = "";
                  }

                window.location.hash = "#result";
            },
            error: function() {
                alert('error');
            },
            url: './upload',
            cache: false
        });

        return false;
    }
    );
});

// getting value from cookie
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

