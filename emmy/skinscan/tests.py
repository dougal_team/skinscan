from django.test import TestCase
from django.test import Client
from .models import Feedback

c = Client()


class IndexTestCase(TestCase):

    def test_index_works(self):
        """
        Cheking that index page loadded without errors
        :return:
        """
        response = c.get('/')
        self.assertEqual(response.status_code, 200)

class FeedbackTestCase(TestCase):

    def test_feedback_save(self):
        """
        Saving feedback to db
        :return:
        """
        response = c.post('/feedback', {"feedbackText":"test"})

        db_feedback = Feedback.objects.get(feedback_text='test')

        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(db_feedback)


    def test_feedback_get_failed(self):
        """
        Testing that GET is not allowed
        :return:
        """
        response = c.get('/feedback')

        self.assertEqual(response.status_code, 405)